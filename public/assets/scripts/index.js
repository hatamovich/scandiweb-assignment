class Product {
	constructor(endpoint) {
		this.host = '/api/';
		this.endpoint = endpoint;
		this.container = document.querySelector('.product-list .row') || 'Not accessible';
	}

	async get() {
		try {
			const response = await fetch(this.host + this.endpoint);
			const result = await response.json();

			return result;
		} catch (error) {
			throw new Error(error.message);
		}
	}

	async insert(callback) {
		try {
			const productList = await this.get();
			
			return callback(productList);
		} catch (reject) {
			console.log('Rejected:', reject.message);
		}
	}
	
	async add(product, backend) {
		try {
			const response = await fetch(this.host + this.endpoint, {
				method: 'POST',
				headers: {'Content-Type': 'application/json'},
				body: JSON.stringify(product)
			});
			
			const result = await response.json();
			return backend(result);
		} catch (reject) {
			console.log('Rejected:', reject.message);
		}
	}
}

const Book = new Product('books');

const Disc = new Product('discs');

const Furniture = new Product('furnitures');