const btnMassDelete = document.querySelector('.btn-massDelete');
const selected = [];


window.addEventListener('click', (e) => {
	const {target} = e;

	if (target.checked === true) {
		selected.push(target.closest('div').dataset.sku);
	}

	return selected;
});

function massDelete(listOfSku) {
	const products = [Disc, Book, Furniture];

	products.forEach(async product => {
		const response = await fetch('/api/' + product.endpoint, {
			method: 'DELETE',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify(listOfSku)
		});

		const result = await response.json();	
	});
}

btnMassDelete.addEventListener('click', () => {
	const countSelected = selected.length;

	if (countSelected) {
		massDelete(selected);
		return setTimeout(() => {
			window.location.href = '/';
		}, 200);
	}


	return ;
});