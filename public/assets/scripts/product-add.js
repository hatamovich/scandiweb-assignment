const form = document.querySelector('.product-add');
const typeSwitcher = document.querySelector('.type-switcher__types');
const typeAttribute = document.querySelector('.type-attribute');

const [SKU, name, price] = form;
let productType = typeSwitcher.value || 'Type is not defined';

const switchType = () => {
	const type = typeSwitcher.value;

	if (type === Disc.endpoint)
		typeAttribute.innerHTML = `
			<span class='message'>Please, provide data of indicated type</span>
			<div>
				<label for='size'>Size (MB)</label>
				<input id='size' type='number' name='size'>
			</div>
			<div class='type-attribute__description'>Please, provide size</div>`;

	else if (type === Book.endpoint) 
		typeAttribute.innerHTML = `
			<span class='message'>Please, provide data of indicated type</span>
			<div>
				<label for='weight'>Weight (KG)</label>
				<input id='weight' type='number' name='weight'>
			</div>
			<div class='type-attribute__description'>Please, provide weight</div>`;

	else if (type === Furniture.endpoint) 
		typeAttribute.innerHTML = `
			<span class='message'>Please, provide data of indicated type</span>
			<div>
				<label for='height'>Height (CM)</label>
				<input id='height' type='number' name='height'>
			</div>
			<div>
				<label for='width'>Width (CM)</label>
				<input id='width' type='number' name='width'>
			</div>
			<div>
				<label for='length'>Length (CM)</label>
				<input id='length' type='number' name='length'>
			</div>
			<div class='type-attribute__description'>Please, provide dimensions</div>`;

	productType = type;
}

typeSwitcher.addEventListener('change', switchType); //Changes product type + content dynamically 

	// const type = switchType();

const save = async event => {
	event.preventDefault();
	const attributes = typeAttribute.querySelectorAll('input');
	
	const array = [
		validate(false, SKU),
		validate(false, name),
		validate(false, price),
		validate(true, attributes, typeAttribute)
	];

	const isValid = array.every(item => item === true);

	async function isUnique(SKU, callBack) {
		const products = [Disc, Book, Furniture];

		products.some(async product => {
			const response = await fetch('/api/' + product.endpoint);
			const result = await response.json();

			const search = result.some(item => item.SKU.toLowerCase() === SKU.toLowerCase());
			callBack(search);
		});
	}

	isUnique(SKU.value, result => {
		if (isValid && !result) {
			switch (productType) {
				case Disc.endpoint:
					Disc.add({
						SKU: SKU.value,
						name: name.value,
						price: price.value,
						size: attributes[0].value 
					}, response => console.log(response));
				break;

				case Book.endpoint:
					Book.add({
						SKU: SKU.value, 
						name: name.value,
						price: price.value,
						weight: attributes[0].value 
					}, response => console.log(response));
				break;

				case Furniture.endpoint: 
					Furniture.add({
						SKU: SKU.value, 
						name: name.value,
						price: price.value,
						height: attributes[0].value,
						width: attributes[1].value,
						length: attributes[2].value
					}, response => console.log(response));
				break;
			}

			setTimeout(() => {
				window.location.href = "/";
			}, 200);
		}
		
		if (result) {
			message(false, {src: SKU, unique: true});
		} else if (SKU.value === '') {
			message(false, {src: SKU});
		} else {
			message(false, {src: SKU, active: false});
		}
	});
};


form.addEventListener('submit', save);