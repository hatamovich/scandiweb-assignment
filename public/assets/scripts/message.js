function message(isAttribute, options = {}) {
	if (isAttribute === true && options.src) {
		const messageHTML = options.src.querySelector('.message');
		messageHTML.classList.add('active');
		messageHTML.textContent = 'Please, provide data of indicated type';

		const isColorSet = options.color ?
		messageHTML.style.color = options.color : false;

		if (options.active === false) {
			messageHTML.classList.remove('active');
			messageHTML.textContent = '';
		}
	}

	if (isAttribute === false) {
		const messageHTML = options.src.parentElement;
		const span = messageHTML.querySelector('.message');

		span.classList.add('active');
		const isColorSet = options.color ? 
		span.style.color = options.color : false;
		span.textContent = 'Please, submit required data';

		if (options.unique === true) {
			span.textContent = 'SKU number must be unique';
		}

		if (options.active === false) {
			span.classList.remove('active');
			span.textContent = '';
		}
	}
}
