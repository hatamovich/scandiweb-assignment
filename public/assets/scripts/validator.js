function validate(isAttribute, input, typeAttribute = false) {
	if (isAttribute === true) {
		for (let i = 0; i < input.length; i++) {
			if (input[i].value === '') {
				message(isAttribute, {src: typeAttribute});
				return false;
			}
			
			message(isAttribute, {src: typeAttribute, active: false});
			return true;
		}
	}

	if (isAttribute === false) {
		if (input.value === '') {
			message(isAttribute, {src: input});
			return false;
		}
		
		message(isAttribute, {src: input, active: false});
		return true;
	}
}
