document.addEventListener('DOMContentLoaded', () => {
	Disc.insert(discs => discs.forEach(disc =>{
		const template = `
		<div class='product col' data-sku='${disc.SKU}'>
			<input id='checkbox-delete' type="checkbox">
			<div class="product-info">
				<p class='product-info__sku'>${disc.SKU}</p>
				<p class='product-info__name'>${disc.name}</p>
				<p class='product-info__price'>${disc.price}.00 $</p>
				<p class='product-info__size'>Size: ${disc.size} MB</p>
			</div>
		</div>`;

		Disc.container.insertAdjacentHTML('afterbegin', template);
	}));
});