document.addEventListener('DOMContentLoaded', () => {
	Furniture.insert(furnitures => furnitures.forEach(furniture => {
		const template = `
		<div class='product col' data-sku='${furniture.SKU}'>
			<input id='checkbox-delete' type="checkbox">	

			<div class="product-info">
				<p class='product-info__sku'>${furniture.SKU}</p>
				<p class='product-info__name'>${furniture.name}</p>
				<p class='product-info__price'>${furniture.price}.00 $</p>
				<p class='product-info__dimensions'>Dim: ${furniture.height}x${furniture.width}x${furniture.length}</p>
			</div>
		</div>`;

		Furniture.container.insertAdjacentHTML('afterbegin', template);
	}));
});