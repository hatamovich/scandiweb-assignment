document.addEventListener('DOMContentLoaded', () => {
	Book.insert(books => books.forEach((book) => {
		const template = `
		<div class='product col'  data-sku='${book.SKU}'>
			<input id='checkbox-delete' type="checkbox">

			<div class="product-info">
				<p class='product-info__sku'>${book.SKU}</p>
				<p class='product-info__name'>${book.name}</p>
				<p class='product-info__price'>${book.price}.00 $</p>
				<p class='product-info__weight'>Weight: ${book.weight} KG</p>
			</div>
		</div>`;

		Book.container.insertAdjacentHTML('afterbegin', template);
	})); 
});