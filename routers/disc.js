const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

const Disc = require('../models/disc');

router.get('/', async (request, response) => {
	try {
		const discs = await Disc.find();
		response.send(discs);
	} catch (reject) {
		response.send(reject.message);
	}
});

router.post('/', async (request, response) => {	
	try {
		const {body} = request;

		response.send(body);
		return await new Disc(body).save();
	} catch (e) {
		console.log(e.message);
	}
});

router.delete('/', async (request, response) => {	
	try {
		const {body} = request;

		response.send(body);
		return await Disc.deleteMany({SKU: {$in: body}});
	} catch (e) {
		console.log(e.message);
	}
})

// Export...
module.exports = router;
