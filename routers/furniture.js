const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

const Furniture = require('../models/furniture');

router.get('/', async (request, response) => {
	try {
		const furnitures = await Furniture.find();
		response.send(furnitures);
	} catch (reject) {
		response.send(reject.message);
	}
});

router.post('/', async (request, response) => {	
	try {
		const {body} = request;

		response.send(body);
		return await new Furniture(body).save();
	} catch (e) {
		console.log(e.message);
	}
});

router.delete('/', async (request, response) => {	
	try {
		const {body} = request;

		response.send(body);
		return await Furniture.deleteMany({SKU: {$in: body}});
	} catch (e) {
		console.log(e.message);
	}
});

// Export...
module.exports = router;

