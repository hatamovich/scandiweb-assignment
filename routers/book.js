const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

const Book = require('../models/book');

router.get('/', async (request, response) => {
	try {
		const listOfBooks = await Book.find();
		response.send(listOfBooks);
	} catch (reject) {
		response.send(reject.message);
	}
});

router.post('/', async (request, response) => {	
	try {
		const {body} = request;

		response.send(body);
		return await new Book(body).save();
	} catch (e) {
		console.log(e.message);
	}
});

router.delete('/', async (request, response) => {	
	try {
		const {body} = request;

		response.send(body);
		return await Book.deleteMany({SKU: {$in: body}});
	} catch (e) {
		console.log(e.message);
	}
});

// Export...
module.exports = router;