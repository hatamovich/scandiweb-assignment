// Required 3rd party packages
const express = require('express');
const mongoose = require('mongoose');
const debug = require('debug')('Backend:');

const app = express();
app.use(express.json());
app.use(express.static('public'));

// Connection to MongoDB...
mongoose.connect('mongodb+srv://hatamovichDB:Murod1995@hatamovichdb.x1ksw.mongodb.net/products', 
{useNewUrlParser: 1, useUnifiedTopology: 1})
.then(() => debug('Connected to MongoDB'))
.catch(() => debug('Connection to MongoDB failed'));

// Required routers...
const discRouter = require('./routers/disc');
app.use('/api/discs', discRouter);

const bookRouter = require('./routers/book');
app.use('/api/books', bookRouter);

const furnitureRouter = require('./routers/furniture');
app.use('/api/furnitures', furnitureRouter);


debug('Environment:', process.env.NODE_ENV);
const port = process.env.PORT || 5000;
app.listen(port, () => debug(`Listening ${port}`));

// --------------------------------------------------
app.get('/', (request, response) => res.send('Dev Env'));