const mongoose = require('mongoose');

const {Schema} = mongoose;
const bookSchema = new Schema({
	SKU: String,
	name: String,
	price: Number,
	weight: Number
});

const Book = mongoose.model('Book', bookSchema, 'books');

// Exports...
module.exports = Book;
