const mongoose = require('mongoose');

const {Schema} = mongoose;
const discSchema = new Schema({
	SKU: {type: String, required: true, length: 9}, 
	name: {type: String, required: true},
	price: Number,
	size: Number
});

const Disc = mongoose.model('Disc', discSchema, 'discs');

// Exports...
module.exports = Disc;
