const mongoose = require('mongoose');

const {Schema} = mongoose;
const furnitureSchema = new Schema({
	SKU: String,
	name: String,
	price: Number,
	height: Number,
	width: Number,
	length: Number
});

const Furniture = mongoose.model('Furniture', furnitureSchema, 'furnitures');

// Exports...
module.exports = Furniture;
